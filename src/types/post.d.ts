type PostProps = {
  title: string
  body: string
  metadata: {
    publishedAt: number
    authorId: number
  }
  id?: number
  author?: {
    name: string
  }
}