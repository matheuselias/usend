import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0'

import Input from '.'

export default {
  title: 'Input',
  component: Input,
} as Meta

export const Default: Story = (args) => <Input {...args} />

Default.args = {
  placeholder: 'Type here'
}