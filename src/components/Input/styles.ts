import styled, { css } from 'styled-components';

export const Wrapper = styled.input`
  ${({ theme }) => css`
    font-family: ${theme.font.family};
    font-size: ${theme.font.sizes.medium};
    padding: ${theme.spacings.xxsmall};
    background: transparent;
    border: 1px solid ${theme.colors.primary};
    border-radius: ${theme.border.radius};
    color: ${theme.colors.gray};
    outline: none;
    width: 90%;
  `};
`