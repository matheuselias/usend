import React from 'react';

import * as Styles from './styles';

const Input = ({...props}: React.InputHTMLAttributes<HTMLInputElement>) => {
  return (
    <Styles.Wrapper {...props}></Styles.Wrapper>
  );
};

export default Input;