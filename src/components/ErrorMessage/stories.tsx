import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import ErrorMessage, { ErrorMessageProps } from '.';

export default {
  title: 'ErrorMessage',
  component: ErrorMessage,
} as Meta;

export const Default: Story<ErrorMessageProps> = (args) => <ErrorMessage {...args} />;

Default.args = {
  error: 'Some Error'
}