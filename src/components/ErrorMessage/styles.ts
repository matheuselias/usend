import styled, { css } from "styled-components";

export const Wrapper = styled.p`
  ${({ theme }) => css`
    color: ${theme.colors.error};
    font-size: ${theme.font.sizes.large};
    font-weight: ${theme.font.bold};
  `};
`;