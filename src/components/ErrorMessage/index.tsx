import React from 'react';

import * as Styles from './styles';

export type ErrorMessageProps = {
  error: string
}

const ErrorMessage = ({ error }: ErrorMessageProps) => {
  return <Styles.Wrapper>{error}</Styles.Wrapper>;
};

export default ErrorMessage;