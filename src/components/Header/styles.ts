import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

export const Wrapper = styled.header`
  ${({ theme }) => css`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    width: 100%;
    padding: ${theme.spacings.xxsmall} ${theme.spacings.medium};
    height: 85px;
  `};
`

export const Logo = styled(Link)`
  ${({ theme }) => css`
    cursor: pointer;
    text-decoration: none;
    font-size: ${theme.font.sizes.huge};
    font-weight: ${theme.font.bold};
    color: ${theme.colors.primary};
  `};
`;