import React from 'react';

import * as Styles from './styles';

const Header = () => {
  return (
    <Styles.Wrapper>
      <Styles.Logo to="/">Usend Blog</Styles.Logo> 
    </Styles.Wrapper>
  );
};

export default Header;