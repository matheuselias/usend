import React from 'react';

import * as Styles from './styles';

export type CardProps = {
  date: number
  title: string
  preview: string
  author: string
  onClick: () => void
}

const Card = ({title, date, preview, author, onClick, ...props}: CardProps) => {
  const formatDate = new Date(date);
  
  return (
    <Styles.Wrapper onClick={onClick} {...props}>
      <Styles.Date>{formatDate.toDateString()} - {author}</Styles.Date>
      <Styles.Title>{title}</Styles.Title>
      <Styles.Preview>{preview}</Styles.Preview>
    </Styles.Wrapper>
  )
};

export default Card;