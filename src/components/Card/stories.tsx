import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0'

import Card, { CardProps } from '.'

export default {
  title: 'Card',
  component: Card,
} as Meta

export const Default: Story<CardProps> = (args) => <Card {...args} />

Default.args = {
  date: 1490010372000,
  title: 'Some Randon Title',
  author: 'Author',
  preview: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita, earum soluta corrupti molestiae explicabo cum inventore qui eveniet'
}