import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    width: 80%;
    padding: ${theme.spacings.small};
    margin: ${theme.spacings.xxsmall} 0;
    background-color: ${theme.colors.secondary};
    color: ${theme.colors.white};
    cursor: pointer;
  `}; 
`;

export const Date = styled.span`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.small};
  `};
`

export const Title = styled.span`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.xxlarge};
    margin-bottom: ${theme.spacings.xsmall};
  `};
`

export const Preview = styled.p`
  ${({ theme }) => css`
    width: 70%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size: ${theme.font.sizes.medium};
  `};
`