import styled, { css, DefaultTheme } from 'styled-components';

import { ButtonProps } from '.'

const wrapperModifier = {
  small: (theme: DefaultTheme) => css`
    height: 2.5rem;
    font-size: ${theme.font.sizes.xsmall};
  `,
  medium: (theme: DefaultTheme) => css`
    height: 3.5rem;
    font-size: ${theme.font.sizes.medium};
  `,
  large: (theme: DefaultTheme) => css`
    height: 5rem;
    font-size: ${theme.font.sizes.large};
  `,
}

export const Wrapper = styled.button<ButtonProps>`
  ${({ theme, size }) => css`
    background-color: ${theme.colors.primary};
    color: ${theme.colors.white};
    border: 0;
    cursor: pointer;
    border-radius: ${theme.border.radius};
    padding: ${theme.spacings.xxsmall};
    text-decoration: none;
    ${!!size && wrapperModifier[size](theme)};
  `}
`