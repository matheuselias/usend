import React, { AnchorHTMLAttributes, ButtonHTMLAttributes } from 'react'

import * as Styles from './styles';

type ButtonTypes =
  | AnchorHTMLAttributes<HTMLAnchorElement>
  | ButtonHTMLAttributes<HTMLButtonElement>

export type ButtonProps = {
  size?: 'small' | 'medium' | 'large'
  children: React.ReactNode,
  onClick: () => void
} & ButtonTypes

const Button = ({children, size = 'medium', onClick}: ButtonProps) => {
  return <Styles.Wrapper size={size} onClick={onClick}>{children}</Styles.Wrapper>
}

export default Button;