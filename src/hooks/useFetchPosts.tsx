import React from 'react';
import axios from 'axios';

import { getAuthorById,  } from '../utils/author';

const useFetchPosts = () => {
  const [loading, setLoading] = React.useState(false);
  const [posts, setPosts] = React.useState<PostProps[] | []>([]);
  const [error, setError] = React.useState('');

  React.useEffect(() => {
    setLoading(true);

    axios.all([
      axios.get('https://run.mocky.io/v3/6f2d303f-50ce-415d-b58e-1a8253b9a4bf'),
      axios.get('https://run.mocky.io/v3/afdbfae9-0bac-41e2-86ac-3e9cfeae5c2c')
    ])
    .then(axios.spread((posts, authors) => {
      const postsData = posts.data;
      const authorsData = authors.data;
      let count = 0;

      const newPosts = postsData.map((post:PostProps) => {
        return {...post, id: count++, author: getAuthorById(authorsData, post.metadata.authorId)}
      });

      setPosts(newPosts);
      setLoading(false);
      setError('');
    }))
    .catch((error) => setError(error.message));
  }, []);

  return { loading, posts, error }

};

export default useFetchPosts;