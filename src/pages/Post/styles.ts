import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    height: calc(100% - 85px);
    justify-content: space-between;
    padding: ${theme.spacings.large};
  `};
`;

export const PostContainer = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    width: 100%;
    align-items: flex-start;
    padding: ${theme.spacings.medium} ${theme.spacings.xxlarge};
  `};
`;

export const Date = styled.span`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.medium};
    margin-bottom: ${theme.spacings.small};
  `};
`;

export const Title = styled.p`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.huge};
    font-weight: 500;
    margin-bottom: ${theme.spacings.medium};
  `};
`;

export const Body = styled.p`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.large};
    width: 80%;
  `};
`

export const Back = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
`;

export const ErrorContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
`;