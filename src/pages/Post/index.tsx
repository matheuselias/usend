import React from 'react';
import {useParams, useHistory} from 'react-router-dom';

import { getPostById } from '../../utils/post';
import useFetchPosts from '../../hooks/useFetchPosts';
import Button from '../../components/Button';
import ErrorMessage from '../../components/ErrorMessage';
import * as Styles from './styles';


const Post = () => {
  const [post, setPost] = React.useState<PostProps>();
  const params: any = useParams();
  const postId = params.postId;
  const { loading, posts, error } = useFetchPosts()
  const history = useHistory();

  React.useEffect(() => {
    if (posts) {
      setPost(getPostById(posts, postId))
    }
  }, [posts, postId])

  const formatDate = (date: number) => {
    const formatDate = new Date(date);

    return formatDate.toDateString();
  }

  const pushBack = () => {
    history.push('/');
  }

  if (loading) {
    return <div></div>
  }

  return (
    <Styles.Wrapper>
      {post ? (
        <Styles.PostContainer>
        <Styles.Date>{formatDate(post.metadata.publishedAt)} - {post?.author?.name } </Styles.Date>
        <Styles.Title>
          {post.title}
        </Styles.Title>

        <Styles.Body>
          {post.body}
        </Styles.Body>
      </Styles.PostContainer>
      )  : null}

      {error ?  (
        <Styles.ErrorContainer>
          <ErrorMessage error={error} />
        </Styles.ErrorContainer>
      ) : null}

      <Styles.Back>
        <Button onClick={pushBack}>Back to list</Button>
      </Styles.Back>
    </Styles.Wrapper>
  )
};

export default Post;