import React from 'react';
import { useHistory } from 'react-router-dom';

import { getPostByAuthor, orderByDate } from '../../utils/post';
import useFetchPosts from '../../hooks/useFetchPosts';
import Input from '../../components/Input';
import Card from '../../components/Card';
import ErrorMessage from '../../components/ErrorMessage';
import Button from '../../components/Button';
import * as Styles from './styles';

const Home = () => {
  const { loading, posts: postsData, error } = useFetchPosts();
  const [searchInput, setSearchInput] = React.useState('');
  const [posts, setPosts] = React.useState<PostProps[] | []>([]);
  const [orderType, setOrderTyper] = React.useState('desc');
  const history = useHistory();

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.currentTarget;
    
    setSearchInput(value);
    
    if (postsData) {
      setPosts(getPostByAuthor(postsData, value))
    }
  }

  React.useEffect(() => {
    setPosts(postsData)
  }, [postsData])

  const pushPost = (id: number) => {
    history.push(`/post/${id}`);
  }
  const order = () => {
    if (orderType === 'desc') {
      setPosts(orderByDate(posts, orderType))
      setOrderTyper('asc');
    } else {
      setPosts(orderByDate(posts, orderType));
      setOrderTyper('desc')
    }
  }

  if (loading) {
    return <div></div>
  }

  return (
    <Styles.Wrapper>
      <Styles.SearchContainer>
        <Input placeholder="Search by author" onChange={onChange} value={searchInput} />
        <Button onClick={order}>{orderType.toUpperCase()}</Button>
      </Styles.SearchContainer>

      {error ?  (
        <Styles.ErrorContainer>
          <ErrorMessage error={error} />
        </Styles.ErrorContainer>
      ) : null}
      
      {posts.length ? posts.map((post: PostProps) => {
        const author = post.author!;
        
        return (
          <Card 
            onClick={() => pushPost(post.id!)} 
            key={post.id} title={post.title} 
            preview={post.body} 
            date={post.metadata.publishedAt}
            author={author.name}
          />
        )})  : null}
    </Styles.Wrapper>
  )
};

export default Home;