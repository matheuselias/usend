export const getPostById = (posts: PostProps[], id: number) => {
  const post = posts.find((post) => post.id == id);

  return post;
};

export const getPostByAuthor = (posts: PostProps[], author: string) => {
  const newPosts = posts.filter((post) => {
    const { name } = post.author!;
    return name.toLowerCase().includes(author.toLowerCase());
  });

  return newPosts;
};

export const orderByDate = (posts: PostProps[], orderType: string) => {
  const newPosts = posts.sort((a, b) => {
    if (orderType === 'asc') {
      return a.metadata.publishedAt - b.metadata.publishedAt
    } else {
      return b.metadata.publishedAt - a.metadata.publishedAt
    }
  });

  return [...newPosts];
}