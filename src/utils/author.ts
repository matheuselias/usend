export const getAuthorById = (authors: AuthorProps[], id: number) => {
  const newAuthor = authors.find((item) => item.id === id);

  return newAuthor;
};
