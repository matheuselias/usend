import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import styled from 'styled-components';

import Header from './components/Header';
import Home from './pages/Home';
import Post from './pages/Post';

const Wrapper = styled.div`
  height: 100%;
`;

const App = () => {
  return (
    <Wrapper>
      <Router>
        <Header />
        <Switch>
          <Route path="/post/:postId">
            <Post />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </Wrapper>
  )
}

export default App;